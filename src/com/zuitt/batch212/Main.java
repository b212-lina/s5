package com.zuitt.batch212;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        Contact john = new Contact();

        john.setName("John Doe");
        john.setNumbers("+639152468596");
        john.setNumbers("+639228547963");
        john.setAddresses("my home in Quezon City");
        john.setAddresses("my office in Makati City");

        Contact jane = new Contact();

        jane.setName("Jane Doe");
        jane.setNumbers("+639162148573");
        jane.setNumbers("+639173698541");
        jane.setAddresses("my home in Caloocan City");
        jane.setAddresses("my office in Pasay City");

        phonebook.setContacts(john);
        phonebook.setContacts(jane);

        phonebook.allPhonebook();



    }

}
