package com.zuitt.batch212;

import java.util.ArrayList;
import java.util.Objects;

public class Phonebook {

    ArrayList<Contact> contacts = new ArrayList<>();
    String display = "";

    public Phonebook() {}

    public Phonebook(Contact contact) {

        this.contacts.add(contact);

    }

    public ArrayList<Contact> getContacts() {

        return this.contacts;

    }

    public void setContacts(Contact contact) {

        this.contacts.add(contact);

    }

    public void allPhonebook() {

        if (this.contacts.size() == 0)

            display = "No contacts";

        else {

            for (Contact contact: contacts) {

                if (display.length() != 0) {

                    display += "\n";

                }

                display = display + contact.getName() + "\n" + "-".repeat(20) + "\n";

                display = display + contact.getName() + " has the following registered numbers:" + "\n";

                if (contact.getNumbers().size() == 0)

                    display = display + "No registered numbers." + "\n";

                else {

                    for (String number: contact.getNumbers()) {

                        display = display + number + "\n";

                    }

                }

                display = display + "-".repeat(35) +"\n";


                display = display +  contact.getName() + " has the following registered addresses:" + "\n";

                if (contact.getAddresses().size() == 0)

                    display = display + "No registered address." + "\n";

                else {

                    for (String address : contact.getAddresses()) {

                        display = display +  address + "\n";

                    }

                }

                display = display + "=".repeat(35);

            }

            System.out.println(display);
        }
    }
}
