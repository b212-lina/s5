package com.zuitt.batch212;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.SplittableRandom;

public class Contact {

    private String name;
    private ArrayList<String> numbers = new ArrayList<>();
    private ArrayList<String> addresses = new ArrayList<>();

    //  Constructors
    public Contact() {}

    public Contact(String name, String number, String address) {

        this.name = name;
        this.numbers.add(number);
        this.addresses.add(address);

    }

    //  getters
    public String getName() {

        return this.name;

    }

    public ArrayList<String> getNumbers() {

        return this.numbers;

    }

    public ArrayList<String> getAddresses() {

        return this.addresses;

    }

    // setters
    public void setName(String name) {

        this.name = name;

    }

    public void setNumbers(String number) {

        this.numbers.add(number);

    }

    public void setAddresses(String address) {

        this.addresses.add(address);

    }

}
